<?php

require_once 'vendor/autoload.php';

use ProDevZone\TemplateMonsterSdk\TemplateMonsterSdk;
use ProDevZone\TemplateMonsterSdk\Method\Xml\Tinfo;
use ProDevZone\TemplateMonsterSdk\Method\Xml\TemplateUpdates;
use ProDevZone\TemplateMonsterSdk\Method\Xml\TemplateXml;
use ProDevZone\TemplateMonsterSdk\Method\Xml\Tinfodir;
use ProDevZone\TemplateMonsterSdk\Method\Screenshot\TemplatesScreenshots4;
use ProDevZone\TemplateMonsterSdk\Method\Catalog\Categories;

//$templateMonsterSdk = new TemplateMonsterSdk('stanislas', 'fed7c666c31d862ec6cc2ad36df5b2b9');

//$tInfoMethod = new Tinfo();
//$tInfoMethod->setFilePath(__DIR__ . DIRECTORY_SEPARATOR . 't_info.zip');
//
//$templateMonsterSdk->api($tInfoMethod);
//if ($templateMonsterSdk->isOk())
//    echo "Successful download" . PHP_EOL;
//else
//    echo "Download error" . PHP_EOL;

//$templateUpdatesMethod = new TemplateUpdates();
//$templateUpdatesMethod->setFilePath(__DIR__ . DIRECTORY_SEPARATOR . 'template_updates.xml');
//$templateUpdatesMethod->setFrom('2016-06-22 00:00:00');
//$templateUpdatesMethod->setTo(date('Y-m-d H:i:s'));
//
//$templateMonsterSdk->api($templateUpdatesMethod);
//if ($templateMonsterSdk->isOk())
//    echo "Successful download" . PHP_EOL;
//else
//    echo "Download error" . PHP_EOL;

//$templateXmlMethod = new TemplateXml;
//$templateXmlMethod->setTemplateNumber(58904);
//
//$templateMonsterSdk->api($templateXmlMethod);
//if ($templateMonsterSdk->isOk())
//    var_dump($templateMonsterSdk->getData());
//else
//    echo "Download error" . PHP_EOL;

//$tInfodirMethod = new Tinfodir();
//$tInfodirMethod->setFilePath(__DIR__ . DIRECTORY_SEPARATOR . 't_info_dir.zip');
//
//$templateMonsterSdk->api($tInfodirMethod);
//if ($templateMonsterSdk->isOk())
//    echo "Successful download" . PHP_EOL;
//else
//    echo "Download error" . PHP_EOL;

//$templatesScreenshots4Method = new TemplatesScreenshots4;
//$templatesScreenshots4Method->setFilePath(__DIR__ . DIRECTORY_SEPARATOR . 'templates_screenshots4.txt');
//$templatesScreenshots4Method->setFrom(0);
//$templatesScreenshots4Method->setFrom(10000);
//
//$templateMonsterSdk->api($templatesScreenshots4Method);
//if ($templateMonsterSdk->isOk())
//    echo "Successful download" . PHP_EOL;
//else
//    echo "Download error" . PHP_EOL;

//$categoriesMethod = new Categories;
//$categoriesMethod->setDelim('|');
//$categoriesMethod->setLinebreak(PHP_EOL);
//$categoriesMethod->setLocale(Categories::LOCALE_EN);
//
//$templateMonsterSdk->api($categoriesMethod);
//if ($templateMonsterSdk->isOk()) {
//    var_dump($templateMonsterSdk->getData());
//    echo PHP_EOL;
//} else
//    echo "Download error" . PHP_EOL;