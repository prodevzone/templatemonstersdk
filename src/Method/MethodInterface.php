<?php

namespace ProDevZone\TemplateMonsterSdk\Method;

/**
 * Interface MethodInterface
 * @package ProDevZone\EnvatoSdk\Method
 * @const URL
 */
interface MethodInterface
{
    public function getProperties();
}