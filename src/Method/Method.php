<?php

namespace ProDevZone\TemplateMonsterSdk\Method;

abstract class Method implements MethodInterface
{
    /**
     * Get all properties of object
     *
     * @return array
     */
    public function getProperties()
    {
        return get_object_vars($this);
    }
}