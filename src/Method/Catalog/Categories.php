<?php

namespace ProDevZone\TemplateMonsterSdk\Method\Catalog;

use ProDevZone\TemplateMonsterSdk\Method\Method;

class Categories extends Method
{
    const URL = 'http://www.templatemonster.com/webapi/categories.php';

    /** @var string Path to file */
    private static $filePath;

    /** @var string */
    protected $linebreak;

    /** @var string */
    protected $delim;

    const LOCALE_EN = 'en';
    const LOCALE_RU = 'ru';

    /** @var string */
    protected $locale;

    /**
     * Set path to file
     * @param $filePath
     */
    public function setFilePath($filePath)
    {
        self::$filePath = $filePath;
    }

    /**
     * Get path to file
     * @return string
     */
    public function getFilePath()
    {
        return self::$filePath;
    }

    /**
     * @return string
     */
    public function getLinebreak()
    {
        return $this->linebreak;
    }

    /**
     * @param string $linebreak
     */
    public function setLinebreak($linebreak)
    {
        $this->linebreak = $linebreak;
    }

    /**
     * @return string
     */
    public function getDelim()
    {
        return $this->delim;
    }

    /**
     * @param string $delim
     */
    public function setDelim($delim)
    {
        $this->delim = $delim;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }
}