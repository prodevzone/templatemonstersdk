<?php

namespace ProDevZone\TemplateMonsterSdk\Method\Screenshot;

use ProDevZone\TemplateMonsterSdk\Method\Method;

class TemplatesScreenshots4 extends Method
{
    const URL = 'http://www.templatemonster.com/webapi/templates_screenshots4.php';

    /** @var string Path to file */
    private static $filePath;

    /** @var string Date & time in "YYYY-MM-DD hh:mm:ss" format */
    protected $from;

    /** @var string Date & time in "YYYY-MM-DD hh:mm:ss" format */
    protected $to;

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * Set path to file
     * @param $filePath
     */
    public function setFilePath($filePath)
    {
        self::$filePath = $filePath;
    }

    /**
     * Get path to file
     * @return string
     */
    public function getFilePath()
    {
        if (is_null(self::$filePath)) {
            self::$filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'templates_screenshots4.txt';
        }

        return self::$filePath;
    }
}