<?php

namespace ProDevZone\TemplateMonsterSdk\Method\Xml;

use ProDevZone\TemplateMonsterSdk\Method\Method;

/**
 * Class TemplateXml
 * @package ProDevZone\TemplateMonsterSdk\Method\Xml
 * @const URL
 * Receive full template info in xml format by the template number.
 */
class TemplateXml extends Method
{
    const URL = 'http://www.templatemonster.com/webapi/template_xml.php';

    /** @var int Template number */
    protected $templateNumber;

    /**
     * @return int
     */
    public function getTemplateNumber()
    {
        return $this->templateNumber;
    }

    /**
     * @param int $templateNumber
     */
    public function setTemplateNumber($templateNumber)
    {
        $this->templateNumber = $templateNumber;
    }
}