<?php

namespace ProDevZone\TemplateMonsterSdk\Method\Xml;

use ProDevZone\TemplateMonsterSdk\Method\Method;

/**
 * Class Tinfodir
 * @package ProDevZone\TemplateMonsterSdk\Method\Xml
 * @const URL
 * Directories Archive.
 */
class Tinfodir extends Method
{
    const URL = 'http://www.templatemonster.com/webapi/xml/t_info_dir.zip';

    /** @var string Path to file */
    private static $filePath;

    /**
     * Set path to file
     * @param $filePath
     */
    public function setFilePath($filePath)
    {
        self::$filePath = $filePath;
    }

    /**
     * Get path to file
     * @return string
     */
    public function getFilePath()
    {
        if (is_null(self::$filePath)) {
            self::$filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 't_info.zip';
        }

        return self::$filePath;
    }
}