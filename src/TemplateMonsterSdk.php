<?php

namespace ProDevZone\TemplateMonsterSdk;

use ProDevZone\TemplateMonsterSdk\Method\Catalog\Categories;
use ProDevZone\TemplateMonsterSdk\Method\MethodInterface;

use ProDevZone\TemplateMonsterSdk\Method\Screenshot\TemplatesScreenshots4;
use ProDevZone\TemplateMonsterSdk\Method\Xml\TemplateUpdates;
use ProDevZone\TemplateMonsterSdk\Method\Xml\Tinfo;
use ProDevZone\TemplateMonsterSdk\Method\Xml\Tinfodir;
use Zend\Http\Client;
use Zend\Http\Response;

class TemplateMonsterSdk
{
    /** @var */
    private $httpClient;

    /** @var Response */
    private $result;

    /** @var string */
    private $login;

    /** @var string */
    private $webApiPassword;
    /**
     * TemplateMonsterSdk constructor
     * @param $login string
     * @param $webApiPassword string
     */
    public function __construct($login, $webApiPassword)
    {
        $this->httpClient = new Client;

        $this->login = $login;
        $this->webApiPassword = $webApiPassword;
    }

    public function api(MethodInterface $method)
    {
        if (is_null($method)) {
            throw new \Exception('No method');
        }

        $url = $method::URL;

        $params = [];
        foreach ($method->getProperties() as $property => $value) {
            $tmp = 'get' . strtoupper($property);
            $params[$this->camelCaseToUnderscore($property)] = str_replace(' ', '%20', $method->$tmp());
        }
        $params['login'] = $this->login;
        $params['webapipassword'] = $this->webApiPassword;

        $url .= '?' . http_build_query($params);
        $this->httpClient->setUri($url);

        $this->httpClient->setUri($url);
        if (in_array(get_class($method), [Tinfo::class, Tinfodir::class])) {
            $this->httpClient->setStream($method->getFilePath())->send();
        }

        $this->result = $this->httpClient->send();

        if ($this->result->isOk() && !is_null($method->getFilePath()) && in_array(get_class($method), [TemplateUpdates::class, TemplatesScreenshots4::class, Categories::class])) {
            file_put_contents($method->getFilePath(), $this->result->getBody());
        }
    }

    public function isOk()
    {
        $result = true;
        if (!$this->result->isOk()) {
            $result = false;
        }

        return $result;
    }

    public function getData()
    {
        return $this->result->getBody();
    }

    /**
     * @param $input
     * @return string
     * @url http://stackoverflow.com/questions/1993721/how-to-convert-camelcase-to-camel-case
     */
    private function camelCaseToUnderscore($input) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}